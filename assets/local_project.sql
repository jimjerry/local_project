-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2018 at 01:15 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `local_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(50) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `first_name`, `last_name`, `email`, `password`) VALUES
(1, 'Donald', 'Corraya', 'jimjerry00b@gmail.com', '202cb962ac59075b964b07152d234b70'),
(2, 'Test', 'D', 'jimjerry00b@gmail.com', '202cb962ac59075b964b07152d234b70'),
(3, 'd', 'dadasd', 'sdadassa@gmail.com', '202cb962ac59075b964b07152d234b70'),
(4, 'test', 'last name', 'test@gmail.com', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Table structure for table `flat_info`
--

CREATE TABLE `flat_info` (
  `id` int(20) NOT NULL,
  `flat_name` varchar(20) DEFAULT NULL,
  `house_id` varchar(20) DEFAULT NULL,
  `status` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flat_info`
--

INSERT INTO `flat_info` (`id`, `flat_name`, `house_id`, `status`) VALUES
(1, 'A1', '2', '1'),
(2, 'A2', '2', '1'),
(3, 'R20', '3', '1'),
(4, 'B1', '2', '1'),
(5, 'R21', '3', '1'),
(6, 'B2', '2', '1');

-- --------------------------------------------------------

--
-- Table structure for table `house_info`
--

CREATE TABLE `house_info` (
  `id` int(10) NOT NULL,
  `house_name` varchar(50) DEFAULT NULL,
  `house_address` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `house_info`
--

INSERT INTO `house_info` (`id`, `house_name`, `house_address`) VALUES
(2, 'South Kafrul', '300/1'),
(3, 'East Kafrul', 'Testing');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(50) NOT NULL,
  `insert_house_rent` varchar(50) DEFAULT NULL,
  `insert_water_bill` varchar(50) DEFAULT NULL,
  `insert_gass_bill` varchar(50) DEFAULT NULL,
  `insert_electricity_bill` varchar(50) DEFAULT NULL,
  `insert_others_bill` varchar(50) DEFAULT NULL,
  `insert_total` varchar(50) DEFAULT NULL,
  `insert_billing_month` varchar(50) DEFAULT NULL,
  `insert_date` varchar(50) DEFAULT NULL,
  `insert_note` varchar(50) DEFAULT NULL,
  `default_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `insert_house_rent`, `insert_water_bill`, `insert_gass_bill`, `insert_electricity_bill`, `insert_others_bill`, `insert_total`, `insert_billing_month`, `insert_date`, `insert_note`, `default_id`) VALUES
(1, '21000', '600', '800', '2500', '0', '24900', 'January', '2018-11-06', 'iktygkitygi', '41'),
(2, '21000', '600', '800', '2500', '0', '24900', 'February', '2018-11-06', 'dadsadsa', '41'),
(3, '21000', '600', '800', '2500', '0', '24900', 'March', '2018-11-29', 'kjlhl,j', '41'),
(4, '21000', '600', '800', '121', '0', '22521', 'April', '2018-12-19', 'ujol', '41'),
(5, '21000', '600', '800', '121', '0', '22521', 'May', '2018-11-28', 'asddasd', '41'),
(6, '21000', '600', '800', '121', '0', '22521', 'January', '2018-10-01', 'o;ou;', '41'),
(7, '15000', '600', '800', '250', '0', '16650', 'January', '2018-11-01', '[;\n\'l\n', '43');

-- --------------------------------------------------------

--
-- Table structure for table `payment_info`
--

CREATE TABLE `payment_info` (
  `id` int(100) NOT NULL,
  `house_rent` varchar(50) NOT NULL,
  `water_bill` varchar(50) NOT NULL,
  `gass_bill` varchar(50) NOT NULL,
  `electricity_bill` varchar(50) NOT NULL,
  `others_bill` varchar(50) NOT NULL,
  `advance` varchar(50) NOT NULL,
  `default_id` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_info`
--

INSERT INTO `payment_info` (`id`, `house_rent`, `water_bill`, `gass_bill`, `electricity_bill`, `others_bill`, `advance`, `default_id`) VALUES
(10, '14000', '600', '800', '568', '0', '10000', 40),
(11, '15000', '600', '800', '900', '0', '10000', 39),
(12, '9500', '600', '800', '125', '25', '10000', 38),
(13, '21000', '600', '800', '121', '0', '25000', 41),
(14, '15000', '600', '800', '250', '0', '15000', 43);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `total_member` varchar(20) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `id_number` varchar(50) NOT NULL,
  `house_no` varchar(50) NOT NULL,
  `flat_no` varchar(50) NOT NULL,
  `start_date` varchar(100) NOT NULL,
  `end_date` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `user_image` varchar(200) NOT NULL,
  `user_nid` varchar(200) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `total_member`, `phone`, `id_number`, `house_no`, `flat_no`, `start_date`, `end_date`, `email`, `user_image`, `user_nid`, `status`) VALUES
(38, 'user2', '4', '1913171116', '6863868', '3', 'R20', '2018-09-04', 'Continue...', 'user2@gmail.com', 'itg1539258589.png', 'nataly1540285585.JPG', 1),
(39, 'Kamrul Islam(Karim)', '5', '01913171116', '6863868', '2', 'A2', '1987-10-01', 'Continue...', 'user1@gmail.com', 'Koala1540450411.jpg', 'nataly1537944072.JPG', 1),
(40, 'user3', '6', '8558498', '746584768978', '2', 'B1', '2018-11-01', 'Continue...', 'user3test@gmail.com', 'Penguins1540355217.jpg', 'Desert1540355217.jpg', 1),
(41, 'user1111', '5', '95989849', '491510494', '3', 'R21', '2018-11-01', 'Continue...', 'user1111@gmail.com', 'model11540725452.png', 'model21540725452.png', 1),
(42, 'user7', '3', '496549641', '49841198', '2', 'B2', '2018-11-01', 'Continue...', 'user7@gmail.com', 'itg1541411475.png', 'nataly1541411475.JPG', 1),
(43, 'user8', '6', '4564654', '4564564', '2', 'A1', '2018-11-01', 'Continue...', 'user8@gmail.com', 'itg1541413533.png', 'nataly1541413533.JPG', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flat_info`
--
ALTER TABLE `flat_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `house_info`
--
ALTER TABLE `house_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_info`
--
ALTER TABLE `payment_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `flat_info`
--
ALTER TABLE `flat_info`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `house_info`
--
ALTER TABLE `house_info`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `payment_info`
--
ALTER TABLE `payment_info`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
