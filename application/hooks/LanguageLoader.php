<?php

class LanguageLoader
{
    function initialize() {
        $ci =& get_instance();
        $ci->load->helper('language'); 
        $this->checkLanguage();
    }
    
    
    
    function checkLanguage(){
            $ci =& get_instance();
        $lan_code=(string)$ci->uri->segment('1');


        if($lan_code!="" and (strlen($lan_code )<4)){  
        $ci->session->set_userdata('lang', $lan_code);
        }else{
            $se_lan=$ci->session->userdata("lang");
            $lan_code= ($se_lan)? $se_lan:"en"; 
        }

         $ci->lang->load('all_lang', $lan_code);

    }  
    
}