<?php
    Class Login extends CI_Controller{
        
        function __construct() {
            parent::__construct();
            
            if( strlen($this->session->userdata("lang"))==2){
            $this->lang->load('all_lang', $this->session->userdata("lang"));
        }else{
            $this->lang->load('all_lang', 'en');
        }
        }
        
        public function index(){
          
            $this->form_validation->set_rules('username', 'username', 'required');
            $this->form_validation->set_rules('password', 'password', 'required');
            
            
            if($this->form_validation->run()){
                $username = $this->input->post('username');
                $password = md5($this->input->post('password'));
                
                $select ='*';
                $table = 'admin';
                $where = array(
                    'email' => $username,
                    'password' => $password
                );
                $resultOfData = $this->Admin_model->getDataForLogin($select, $table, $where);
                
                if($resultOfData){
                    
                    $userData = array(
                        'current_user_email' => $resultOfData->email,
                        'current_user_first_name' => $resultOfData->first_name,
                        'current_user_last_name' => $resultOfData->last_name,
                    );
                    
                    $this->session->set_userdata($userData);
                    $this->session->set_userdata('lang', 'en');
                    redirect('Admin');
                }else{  
                    $data["error"]="Invalid Username and Password combination";                    
                    $this->load->view('admin/login_page', $data);  
                    return FALSE;
                }
                
            }
            if (!$this->session->userdata("current_user_email")){
                $this->load->view('admin/login_page');                
            }else{  
                 redirect('admin');
               // $data['home_page'] = 'home_page';
               // $this->load->view('admin/dashboard_template', $data);
            }     
        }
        
        public function loggout(){
            $this->session->unset_userdata('current_user_email');
            $this->session->unset_userdata('current_user_first_name');
            $this->session->unset_userdata('current_user_last_name');

            $this->session->sess_destroy();
            redirect('Login');
        }
    }
?>