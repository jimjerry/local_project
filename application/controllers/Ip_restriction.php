<?php

Class Ip_restriction extends CI_Controller {

    function index() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';        
        echo '<input type="hidden" value="' . $ipaddress . '">';
        if ($ipaddress == '127.0.0.1' || $ipaddress == '138.201.174.107' || $ipaddress == '213.133.102.25' || $ipaddress == '178.137.114.125' || $ipaddress == '109.225.28.136' || $ipaddress == '234.123.201.138') {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

?>