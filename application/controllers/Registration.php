<?php
    Class Registration extends CI_Controller{
        public function index() {
            
            $this->form_validation->set_rules('first_name', 'first_name', 'required');
            $this->form_validation->set_rules('last_name', 'last_name', 'required');
            $this->form_validation->set_rules('email', 'email', 'required');
            $this->form_validation->set_rules('password', 'password', 'required');
            $this->form_validation->set_rules('c_password', 'confirm password', 'required|matches[password]');
            
            if($this->form_validation->run()){
                $first_name = $this->input->post('first_name');
                $last_name = $this->input->post('last_name');
                $email = $this->input->post('email');
                $password = md5($this->input->post('c_password'));
                
                $table = 'admin';
                $attr = array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'password' => $password,
                );
                $result = $this->Admin_model->DataInsert($table, $attr);
                if($result){
                    redirect(base_url().'Login');
                }
                
            }
            
            $this->load->view('admin/new_registration_page');
        }
    }
?>