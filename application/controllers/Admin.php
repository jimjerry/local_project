<?php

Class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();



        if (!$this->session->userdata("current_user_email")) {
            redirect('Login/loggout');
        }
       
    }   
    
    
    public function urlRedirect($lang) {
        $this->session->set_userdata('lang', $lang);         
        redirect($_SERVER['SERVER_NAME']);
    }

    public function index() {
        $data['home_page'] = 'home_page';
        $this->load->view('admin/dashboard_template', $data);
    }

    public function add_new_user() {

        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('total_members', 'total_members', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');
        $this->form_validation->set_rules('id_number', 'id_number', 'required');
        $this->form_validation->set_rules('house_no', 'Flat no', 'required');
        $this->form_validation->set_rules('flat_no', 'Flat no', 'required');
        $this->form_validation->set_rules('start_date', 'Start Date', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $data['message'] = "";
        if ($this->form_validation->run()) {

            $name = $this->input->post('name');
            $total_members = $this->input->post('total_members');
            $phone = $this->input->post('phone');
            $id_number = $this->input->post('id_number');
            $house_no = $this->input->post('house_no');
            $flat_no = $this->input->post('flat_no');
            $start_date = $this->input->post('start_date');
            $email = $this->input->post('email');
            //$status = $this->input->post('email');

            $user_image = $_FILES['user_image']['name'];
            $nid = $_FILES['nid']['name'];

            $image_name = explode('.', $user_image);
            $image_name_nid = explode('.', $nid);

            $image_ex = end($image_name);
            $image_ex_nid = end($image_name_nid);

            $image_name[0] = $image_name[0] . time() . "." . $image_ex;
            $image_name_nid[0] = $image_name_nid[0] . time() . "." . $image_ex_nid;

            $_FILES["user_image"]["name"] = $image_name[0];
            $_FILES["nid"]["name"] = $image_name_nid[0];

            $user_image = $image_name[0];
            $nid = $image_name_nid[0];
            $config['upload_path'] = './assets/images/users_images/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 100000;

            $fullPath = $config['upload_path'] . $user_image;
            $fullPath_nid = $config['upload_path'] . $nid;
            $user_image_name = "";
            $nid_image_name = "";

            /* changing flat status */
            if ($flat_no != "") {

                $select = 'house_info.id, house_info.house_name, flat_info.flat_name, flat_info.house_id, flat_info.`status`';
                $from = 'house_info';
                $whr = array(
                    'house_id' => $house_no,
                    'flat_name' => $flat_no
                );
                $join = array('flat_info', 'house_info.id = flat_info.house_id', 'INNER');

                $flat_house_Info = $this->Admin_model->getDataForLogin($select, $from, $whr, $join);
                //echo $this->db->last_query(); exit;
            } else {
                echo "Blak";
                exit();
            }




            if (file_exists($fullPath)) {
                $data['error_image'] = 'File already exits. Please rename your file/ Image';
            } else {

                $this->load->library('upload', $config);

                $imge_upload = $this->upload->do_upload('user_image');
                if ($imge_upload == "") {
                    $data['error_image'] = "Image upload failed";
                } else {
                    if ($this->upload->display_errors()) {
                        $data['error_image'] = array('error' => $this->upload->display_errors());
                    } else {

                        $user_image_name = $user_image;
                    }
                }


                $nid_upload = $this->upload->do_upload('nid');
                if ($nid_upload == "") {
                    $data['error_nid'] = "NID  upload failed";
                } else {
                    if ($this->upload->display_errors()) {
                        $data['error_image'] = array('error' => $this->upload->display_errors());
                    } else {
                        $nid_image_name = $nid;
                    }
                }
            }

            $data['message'] = "<b style='color:red'> Data save failed. [File not supported]</b>";

            if ($user_image_name != "" and $nid_image_name != "") {
                $userData = array(
                    'name' => $name,
                    'total_member' => $total_members,
                    'phone' => $phone,
                    'id_number' => $id_number,
                    'house_no' => $house_no,
                    'flat_no' => $flat_no,
                    'start_date' => $start_date,
                    'end_date' => "Continue...",
                    'email' => $email,
                    'user_image' => $user_image_name,
                    'user_nid' => $nid_image_name,
                    'status' => '1'
                );

                $result = $this->Admin_model->DataInsert('users', $userData);
                $table = 'flat_info';

                $where = array(
                    'flat_name' => $flat_no,
                    'house_id' => $house_no
                );
                $attr = array(
                    'status' => '1'
                );
                $statusUpdate = $this->Admin_model->updateInfo($table, $attr, $where);

                $data['message'] = "<b style='color:green'> Data save succesfully done</b>";
            }
            if ($result) {
                redirect('Admin/users_list');
            } else {
                echo "Something may wrong";
            }
        }

        $table = 'house_info';
        $select = '*';
        $data['result'] = $this->Admin_model->getAllDataFromTable($table, $select);

        $userEmail = $this->session->userdata("current_user_email");
        if ($userEmail == 'jimjerry00b@gmail.com') {
            $data['add_new_user_page'] = 'admin/add_new_user_page';
            $this->load->view('admin/dashboard_template', $data);
        } else {
            redirect('Admin');
        }
    }

    public function users_list() {
        $select = 'users.id, users.`name`, users.total_member, users.phone, users.id_number, users.house_no, users.flat_no, users.start_date, users.end_date, users.email, users.user_image, users.user_nid,
users.`status`, house_info.house_name, house_info.house_address';
        $table = 'users';
        $whr = array(
            'users.status' => '1'
        );
        $join = array('house_info', 'users.house_no = house_info.id', 'INNER');
        $data['result'] = $this->Admin_model->getAllDataFromTable($table, $select, $whr, $groupBy = '', $join);

        $data['users_list_page'] = 'admin/users_list_page';
        $this->load->view('admin/dashboard_template', $data);
        //echo $this->db->last_query();exit;
    }

    public function deactive_users_list() {
        $select = 'users.id, users.`name`, users.total_member, users.phone, users.id_number, users.house_no, users.flat_no, users.start_date, users.end_date, users.email, users.user_image, users.user_nid,
users.`status`, house_info.house_name, house_info.house_address';
        $table = 'users';
        $whr = array(
            'users.status' => '0'
        );
        $join = array('house_info', 'users.house_no = house_info.id', 'INNER');
        $data['result'] = $this->Admin_model->getAllDataFromTable($table, $select, $whr, $groupBy = '', $join);

        $data['users_list_page'] = 'admin/users_list_page';
        $this->load->view('admin/dashboard_template', $data);
        //echo $this->db->last_query();exit;
    }

    public function user_delete() {
        $deleteID = $this->input->post('delid');

        $table = 'users';
        $resutl = $this->Admin_model->deleteData($table, $deleteID);
        if ($resutl == TRUE) {
            echo 'done';
        } else {
            echo "fail";
        }
    }

//    public function deactive_user() {
//        echo "deactive_user";
//    }
//
//    public function active_user() {
//        echo "active_user";
//    }

    public function add_house() {

        $this->form_validation->set_rules('house_name', 'House name', 'required');
        $this->form_validation->set_rules('house_address', 'House address', 'required');

        if ($this->form_validation->run()) {
            $house_name = $this->input->post('house_name');
            $house_address = $this->input->post('house_address');

            $data = array(
                'house_name' => $house_name,
                'house_address' => $house_address
            );
            $result = $this->Admin_model->DataInsert('house_info', $data);
        }

        $data['add_new_house_page'] = 'admin/add_new_house_page';
        $this->load->view('admin/dashboard_template', $data);
    }

    public function total_house() {

        $table = 'house_info';
        $select = 'house_info.id, house_info.house_name, house_info.house_address, GROUP_CONCAT(flat_info.flat_name,"___",flat_info.`status`) as flat_name,flat_info.`status`';
        $join = array('flat_info', 'house_info.id = flat_info.house_id', 'INNER');
        $groupBy = 'house_info.id';
        $data['result'] = $this->Admin_model->getAllDataFromTable($table, $select, null, $groupBy, $join);

        $data['total_house_page'] = 'admin/total_house_page';
        $this->load->view('admin/dashboard_template', $data);
    }

    public function add_flat() {

        $this->form_validation->set_rules('flat_name', 'flat name', 'required');
        $this->form_validation->set_rules('house_id', 'flat value', 'required');


        if ($this->form_validation->run()) {
            $flat_name = $this->input->post('flat_name');
            $house_id = $this->input->post('house_id');

            $data = array(
                'flat_name' => $flat_name,
                'house_id' => $house_id,
                'status' => '0'
            );
            $result = $this->Admin_model->DataInsert('flat_info', $data);
        }

        $table = 'house_info';
        $select = '*';
        $data['house'] = $this->Admin_model->getAllDataFromTable($table, $select);

        $data['add_new_flat_page'] = 'admin/add_new_flat_page';
        $this->load->view('admin/dashboard_template', $data);
    }

    public function getFlatData() {
        $id = $this->input->post('houseId');
        $table = 'flat_info';
        $select = '*';
        $where = array(
            'house_id' => $id,
            'status' => '0'
        );
        $result = $this->Admin_model->getAllDataFromTable($table, $select, $where);
        //echo $this->db->last_query(); exit;
        $val = '';
        foreach ($result as $results) {
            $val .= '<option>' . $results->flat_name . '</option>';
        }

        echo(json_encode($val));
    }

    public function update_user_info() {
        $userID = $this->input->get('id');


        $table = 'users';
        $select = '*';
        $where = array(
            'id' => $userID
        );

        $data['results'] = $this->Admin_model->getDataForLogin($select, $table, $where);
        //echo "<pre>";print_r($data['results']);exit;
        //echo $this->db->last_query();exit;

        /* Update data and validation */
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('total_members', 'total_members', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');
        $this->form_validation->set_rules('id_number', 'id_number', 'required');
        $this->form_validation->set_rules('house_no', 'house_no', 'required');
        $this->form_validation->set_rules('flat_no', 'flat_no', 'required');
        $this->form_validation->set_rules('status', 'status', 'required');
        $this->form_validation->set_rules('start_date', 'start_date', 'required');
        $this->form_validation->set_rules('end_date', 'end_date', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');

        if ($this->form_validation->run()) {
            $name = $this->input->post('name');
            $total_members = $this->input->post('total_members');
            $phone = $this->input->post('phone');
            $id_number = $this->input->post('id_number');
            $house_no = $this->input->post('house_no');
            $flat_no = $this->input->post('flat_no');
            $status = $this->input->post('status');
            $start_date = $this->input->post('start_date');
            $end_date = $this->input->post('end_date');
            $email = $this->input->post('email');

            $user_image = $_FILES['user_image']['name'];
            $nid_card = $_FILES['nid']['name'];


            /* user image exploding */
            $image_divided = explode('.', $user_image);
            if ($image_divided[0] != "" and $image_divided[1] != "") {
                $user_image = $image_divided[0] . time() . "." . $image_divided[1];
            }
            /* NID Card exploding */
            $nid_card_divided = explode('.', $nid_card);
            if ($nid_card_divided[0] != "" and $nid_card_divided[1] != "") {
                $nid_card = $nid_card_divided[0] . time() . "." . $nid_card_divided[1];
            }

            $userImage = $user_image;
            $nidCard = $nid_card;

            $_FILES['user_image']['name'] = $user_image;
            $_FILES['nid']['name'] = $nid_card;

            $config['upload_path'] = './assets/images/users_images/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 100000;

            $fullPath = $config['upload_path'] . $userImage;
            $fullPath = $config['upload_path'] . $nidCard;

            $this->load->library('upload', $config);

            $img_upload = $this->upload->do_upload('user_image');
            $nid_img_upload = $this->upload->do_upload('nid');

            if ($img_upload == "") {
                $data['error_image'] = "Image upload failed";
                //echo "image upload fail";exit();
            } else {
                if ($this->upload->display_errors()) {
                    $data['error_image'] = array('error' => $this->upload->display_errors());
                } else {

                    //$user_image_name= $user_image;
                }
            }

            if ($nid_img_upload == "") {
                $data['error_image'] = "Image upload failed";
            } else {
                if ($this->upload->display_errors()) {
                    $data['error_image'] = array('error' => $this->upload->display_errors());
                } else {
                    //$user_image_name= $user_image;
                }
            }

            if ($userImage == "") {
                $userImage = $data['results']->user_image;
            } else {
                $userImage;
            }

            if ($nidCard == "") {
                $nidCard = $data['results']->user_nid;
            } else {
                $nidCard;
            }
            //echo $nidCard;exit;

            /* floor status changing if deactive set */



            $select = "*";
            $table2 = "users";
            $whr = array(
                'status' => $data['results']->status,
                'flat_no' => $flat_no,
                'house_no' => $house_no
            );

            $HouseCurrentStatus = $this->Admin_model->getAllDataFromTable($table2, $select, $whr);

            if ($HouseCurrentStatus) {

                $whr = array(
                    'flat_name' => $flat_no,
                    'house_id' => $house_no
                );

                $table = 'flat_info';
                $userData = array(
                    'status' => $status
                );

                $this->Admin_model->updateInfo($table, $userData, $whr);
            }

            /* floor status changing if deactive set end */

            $table_UserTable = 'users';
            $whr_UserTable = array(
                'id' => $userID
            );
            $userData_UserTable = array(
                'name' => $name,
                'total_member' => $total_members,
                'phone' => $phone,
                'id_number' => $id_number,
                'house_no' => $house_no,
                'flat_no' => $flat_no,
                'status' => $status,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'user_image' => $userImage,
                'user_nid' => $nidCard,
                'email' => $email
            );
            $result = $this->Admin_model->updateInfo($table_UserTable, $userData_UserTable, $whr_UserTable);

            if ($result) {
                redirect('Admin/users_list');
            } else {
                echo 'Something wrong';
                exit();
            }
        }

        /* Update data and validation END */





        /* getting house_info from table where house status = 0 */
        $table = 'house_info';
        $select = '*';
        $data['house_info'] = $this->Admin_model->getAllDataFromTable($table, $select);

        $table = 'flat_info';
        $select = '*';
        $where = array(
            'house_id' => $data['results']->house_no,
            'status' => 0
        );
        $data['flat_info'] = $this->Admin_model->getAllDataFromTable($table, $select, $where);
        //echo "<pre>" ;print_r($data['flat_info']);exit;

        /* going to view page */
        $data['update_user_page'] = 'admin/update_user_page';
        $this->load->view('admin/dashboard_template', $data);
    }

    public function user_profile($id = "") {

        $id = $this->input->get('id');

        $userTableSelect = 'users.id, users.email, users.name, users.total_member, users.phone, users.id_number, users.house_no, users.flat_no, users.start_date, users.end_date, users.user_image, users.user_nid, users.status, house_info.house_name, house_info.house_address';
        $userTableFrom = 'users';
        $joinforBasicInfo = array('house_info', 'users.house_no = house_info.id', 'INNER');
        $userTableWhr = array(
            'users.id' => $id
        );

        $data['basicInfo'] = $this->Admin_model->getDataForLogin($userTableSelect, $userTableFrom, $userTableWhr, $joinforBasicInfo);

        $select = "*";
        $FROM = 'payment_info';
        $whr = array(
            'default_id' => $id
        );


        // getting data for data table

        $select_Datatable = '*';
        $table_Datatable = 'payment';
        $whr_Datatable = array(
            'default_id' => $id
        );
        $data['tableData'] = $this->Admin_model->getAllDataFromTable($table_Datatable, $select_Datatable, $whr_Datatable);

        $data['gettingData'] = $this->Admin_model->getDataForLogin($select, $FROM, $whr);
        //print_r($data['gettingData']);exit;
        $data['default_id'] = $id;
        $data['user_profile'] = 'admin/user_profile_page';
        $this->load->view('admin/dashboard_template', $data);
    }

    public function update_user_profile_payment() {
        $this->form_validation->set_rules('house_rent', 'house_rent', 'required');
        $this->form_validation->set_rules('water_bill', 'water_bill', 'required');
        $this->form_validation->set_rules('gass_bill', 'gass_bill', 'required');
        $this->form_validation->set_rules('electricity_bill', 'electricity_bill', 'required');
        $this->form_validation->set_rules('others_bill', 'others_bill', 'required');
        $this->form_validation->set_rules('advance', 'advance', 'required');
        $this->form_validation->set_rules('default_id', 'default_id', 'required');
        $this->form_validation->set_rules('default_id', 'default_id', 'required');
        $this->form_validation->set_rules('electric_meter_ac', 'electric_meter_ac', 'required');
        $this->form_validation->set_rules('electric_meter_no', 'electric_meter_no', 'required');




        if ($this->form_validation->run()) {
            $houseRent = $this->input->post('house_rent');
            $water_bill = $this->input->post('water_bill');
            $gass_bill = $this->input->post('gass_bill');
            $electricity_bill = $this->input->post('electricity_bill');
            $others_bill = $this->input->post('others_bill');
            $advance = $this->input->post('advance');
            $default_id = $this->input->post('default_id');
            $electric_meter_ac = $this->input->post('electric_meter_ac');
            $electric_meter_no = $this->input->post('electric_meter_no');

            $userData = array(
                'house_rent' => $houseRent,
                'water_bill' => $water_bill,
                'gass_bill' => $gass_bill,
                'electricity_bill' => $electricity_bill,
                'others_bill' => $others_bill,
                'advance' => $advance,
                'default_id' => $default_id,
                'electric_meter_ac' => $electric_meter_ac,
                'electric_meter_no' => $electric_meter_no
            );

            $select = "*";
            $FROM = 'payment_info';
            $whr = array(
                'default_id' => $default_id
            );

            $selectID = $this->Admin_model->getDataForLogin($select, $FROM, $whr);

            if ($selectID) {

                $insertData = $this->Admin_model->updateInfo($FROM, $userData, $whr);
            } else {
                $insertData = $this->Admin_model->DataInsert('payment_info', $userData);
            }
        }

        echo json_encode($insertData);
    }

    public function payment() {

        $this->form_validation->set_rules('insert_house_rent', 'insert_house_rent', 'required');
        $this->form_validation->set_rules('insert_water_bill', 'insert_water_bill', 'required');
        $this->form_validation->set_rules('insert_gass_bill', 'insert_gass_bill', 'required');
        $this->form_validation->set_rules('insert_electricity_bill', 'insert_electricity_bill', 'required');
        $this->form_validation->set_rules('insert_others_bill', 'insert_others_bill', 'required');
        $this->form_validation->set_rules('insert_total', 'insert_total', 'required');
        $this->form_validation->set_rules('insert_billing_month', 'insert_billing_month', 'required');
        $this->form_validation->set_rules('insert_date', 'insert_date', 'required');
        $this->form_validation->set_rules('insert_note', 'insert_note', '');

        if ($this->form_validation->run()) {
            $insert_house_rent = $this->input->post('insert_house_rent');
            $insert_water_bill = $this->input->post('insert_water_bill');
            $insert_gass_bill = $this->input->post('insert_gass_bill');
            $insert_electricity_bill = $this->input->post('insert_electricity_bill');
            $insert_others_bill = $this->input->post('insert_others_bill');
            $insert_total = $this->input->post('insert_total');
            $insert_billing_month = $this->input->post('insert_billing_month');
            $insert_date = $this->input->post('insert_date');
            $insert_note = $this->input->post('insert_note');
            $defaultID = $this->input->post('defaultID');

            $table = 'payment';

            $userData = array(
                'insert_house_rent' => $insert_house_rent,
                'insert_water_bill' => $insert_water_bill,
                'insert_gass_bill' => $insert_gass_bill,
                'insert_electricity_bill' => $insert_electricity_bill,
                'insert_others_bill' => $insert_others_bill,
                'insert_total' => $insert_total,
                'insert_billing_month' => $insert_billing_month,
                'insert_date' => $insert_date,
                'insert_note' => $insert_note,
                'default_id' => $defaultID
            );

            $insertData = $this->Admin_model->DataInsert($table, $userData);
        }
        echo json_encode($insertData);
    }

    public function dataTable_data() {
        $id = $this->input->post('defaultID');
        $draw = (int) $this->input->post('draw');
        $select_Datatable = '*';
        $table_Datatable = 'payment';
        $whr_Datatable = array(
            'default_id' => $id
        );
        $tableData = $this->Admin_model->getAllDataFromTable2($table_Datatable, $select_Datatable, $whr_Datatable);
        $recordsTotal = count($tableData);
        $dataresult = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsTotal,
            'data' => $tableData
        );
        echo json_encode($dataresult);
    }

    public function visitors_count() {


// Script Online Users and Visitors - https://coursesweb.net/php-mysql/
        if (!isset($_SESSION))
            session_start(); // start Session, if not already started

        $filetxt = 'userson.txt'; // the file in which the online users /visitors are stored
        $timeon = 10; // number of secconds to keep a user online
        $sep = '^^'; // characters used to separate the user name and date-time
        $vst_id = '-vst-'; // an identifier to know that it is a visitor, not logged user

        /*
          If you have an user registration script,
          replace $_SESSION['nume'] with the variable in which the user name is stored.
          You can get a free registration script from: https://coursesweb.net/php-mysql/register-login-script-users-online_s2
         */

// get the user name if it is logged, or the visitors IP (and add the identifier)
        $uvon = isset($_SESSION['nume']) ? $_SESSION['nume'] : $_SERVER['SERVER_ADDR'] . $vst_id;

        $rgxvst = '/^([0-9\.]*)' . $vst_id . '/i'; // regexp to recognize the line with visitors
        $nrvst = 0; // to store the number of visitors
// sets the row with the current user /visitor that must be added in $filetxt (and current timestamp)
        $addrow[] = $uvon . $sep ;

// check if the file from $filetxt exists and is writable
        if (is_writable($filetxt)) {
            // get into an array the lines added in $filetxt
            $ar_rows = file($filetxt, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
            $nrrows = count($ar_rows); // number of rows
            // if there is at least one line, parse the $ar_rows array
            if ($nrrows > 0) {
                for ($i = 0; $i < $nrrows; $i++) {
                    // get each line and separate the user /visitor and the timestamp
                    $ar_line = explode($sep, $ar_rows[$i]);

                    // add in $addrow array the records in last $timeon seconds
                    if ($ar_line[0] != $uvon && (intval($ar_line[1]) + $timeon) >= time()) {
                        $addrow[] = $ar_rows[$i];
                    }
                }
            }
        }

        $nruvon = count($addrow); // total online
        $usron = ''; // to store the name of logged users
// traverse $addrow to get the number of visitors and users
        for ($i = 0; $i < $nruvon; $i++) {
            if (preg_match($rgxvst, $addrow[$i]))
                $nrvst++; // increment the visitors
            else {
                // gets and stores the user's name
                $ar_usron = explode($sep, $addrow[$i]);
                $usron .= '<br/> - <i>' . $ar_usron[0] . '</i>';
            }
        }
        $nrusr = $nruvon - $nrvst; // gets the users (total - visitors)
// the HTML code with data to be displayed
        $reout = '<div id="uvon"><h4>Online: ' . $nruvon . '</h4>Visitors: ' . $nrvst . '<br/>Users: ' . $nrusr . $usron . '</div>';

// write data in $filetxt
        if (!file_put_contents($filetxt, implode("\n", $addrow)))
            $reout = 'Error: Recording file not exists, or is not writable';

// if access from <script>, with GET 'uvon=showon', adds the string to return into a JS statement
// in this way the script can also be included in .html files
        if (isset($_GET['uvon']) && $_GET['uvon'] == 'showon')
            $reout = "document.write('$reout');";

        echo $reout; // output /display the result
    }

}

?>