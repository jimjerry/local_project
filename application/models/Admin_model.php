<?php

Class Admin_model extends CI_Model {

    public function DataInsert($table, $data) {
        $result = $this->db->insert($table, $data);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getDataForLogin($select, $table, $where, $join = '') {
        $this->db->from($table);
        $this->db->select($select);
        $this->db->where($where);
        if ($join) {
            $this->db->join($join[0], $join[1], $join[2]);
        }
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }
    
    public function getAllDataFromTable($table, $select, $whr='', $groupBy='', $join='') {
        $this->db->select($select);
        $this->db->from($table);
        if($join){
            $this->db->join($join[0], $join[1], $join[2]);
        }
        if($groupBy){
            $this->db->group_by($groupBy); 
        }
        if ($whr) {
            $this->db->where($whr);
        }
        $result = $this->db->get();

        if ($result) {
            return $result->result();
        } else {
            return FALSE;
        }
    }
    
    public function getAllDataFromTable2($table, $select, $whr='') {
        $this->db->select($select);
        $this->db->from($table);
        
        if ($whr) {
            $this->db->where($whr);
        }
        $result = $this->db->get();

        if ($result) {
            return $result->result_array();
        } else {
            return FALSE;
        }
    }

    public function deleteData($table, $id) {
        $this->db->delete($table, array('id' => $id));

        if ($this->db->affected_rows() == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function updateInfo($table, $userData, $whr) {
        //$this->db->set($userData);
        $this->db->where($whr);
        $result = $this->db->update($table, $userData);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function adsdads($table, $userData, $whr) {
        $query = 'SELECT'.$table.'FROM users WHERE id = ?';
        $this->db->query($query, array($whr));
    }
    

}
?>

