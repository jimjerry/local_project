<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Data Table Example</div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Total Member</th>
                        <th>Phone</th>
                        <th>Nid/ID number</th>
                        <th>House name</th>
                        <th>Flat no</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Email</th>
                        
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Total Member</th>
                        <th>Phone</th>
                        <th>Nid/ID number</th>
                        <th>House name</th>
                        <th>Flat no</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Email</th>                        
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                        foreach ($result as $datarow){
                    ?>
                    <tr id="<?= 'row_'.$datarow->id;?>">
                        <td class="text-center">
                            <b style=""><?= $datarow->name?></b>
                            <br>
                            <img style="box-shadow: 0px 1px 12px #888888;" src="<?= base_url()."assets/images/users_images/".$datarow->user_image?>" width="70">
                        </td>
                        <td><?= $datarow->total_member?></td>
                        <td><?= $datarow->phone?></td>
                        <td><?= $datarow->id_number?></td>
                        <td><?= $datarow->house_name?></td>
                        <td><?= $datarow->flat_no?></td>
                        <td><?= $datarow->start_date?></td>
                        <td><?= $datarow->end_date?></td>
                        <td><?= $datarow->email?></td>
                        <!--<td><img src="<?= base_url()."assets/images/users_images/".$datarow->user_image?>" width="50"></td>-->
                        <td>
                            
                                <?php
                                    if($datarow->status == '0'){
                                        echo "<b class='text-danger'> Not active </b>";
                                    }else{
                                        echo "<b class='text-success'>Active </b>";;
                                    }
                                ?>
                            </b>
                        </td>
                        <th>
                            <a href="<?= base_url()?>Admin/update_user_info?id=<?= $datarow->id?>"><button class="btn btn-sm btn-success">Edit</button></a>
                            <a href="<?= base_url()?>Admin/user_profile?id=<?= $datarow->id?>"><button class="btn btn-sm btn-dark">View</button></a>
                            <button class="btn btn-danger btn-sm" id="<?= $datarow->id;?>" onclick="delete_data(this.id)">Delete</button>
                        </th>
                    </tr>
                    
                    <?php
                        }
                    ?>
                   
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>

<script>
    function delete_data(del_id){
        var url = '<?= base_url()?>Admin/user_delete';        
        var id = del_id;     
        var result = confirm("are sure want to delete this?");
        
        if(result){
            $.post(url,{delid:id},function(r){
           
                if(r.trim().toString() == 'done')
                {
                  $("#row_"+id).fadeOut();
                }
            });
        }
        
    }
    
</script>
