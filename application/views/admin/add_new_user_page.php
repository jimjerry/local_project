<div class="card card-register mx-auto mt-5">
    <div class="card-header">Register New User</div>    
    <div class="text-center">
        <?=$message?>
        <?php // echo $this->session->flashdata('message')?>
    <?php 
    if(isset($user_img_error)){
        echo "<span class='text-danger'>".$user_img_error."</span>";        
    }
    ?>
    
    <?php 
    if(isset($errorMsgNID)){
        echo "<span class='text-danger'>".$errorMsgNID."</span>";        
    }
    ?>
    </div>
    
    <div class="card-body">
        <form action="" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-6">                        
                        <div class="form-label-group">                                
                            <input name="name" id="name" type="text" class="form-control" placeholder="Name">
                            
                            <label for="name">
                                <?php
                                    if(form_error('name')){
                                        echo "<span class='text-danger'>".form_error('name')."</span>";
                                    }else{
                                        echo "Name";
                                    }
                                ?>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-label-group">
                            <input name="total_members" id="total_members" type="text" class="form-control" placeholder="Total Members">
                            <label for="total_members">
                                <?php
                                    if(form_error('name')){
                                        echo "<span class='text-danger'>".form_error('total_members')."</span>";
                                    }else{
                                        echo "Total Members";
                                    }
                                ?>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="form-label-group">
                            <input name="phone" id="phone" type="text" class="form-control" placeholder="Phone">
                            <label for="phone">
                                <?php
                                    if(form_error('phone')){
                                        echo "<span class='text-danger'>".form_error('total_members')."</span>";
                                    }else{
                                        echo "Phone";
                                    }
                                ?>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-label-group">
                            <input name="id_number" id="id_number" type="text" class="form-control" placeholder="Id number">
                            <label for="id_number">                                
                                <?php
                                    if(form_error('id_number')){
                                        echo "<span class='text-danger'>".form_error('total_members')."</span>";
                                    }else{
                                        echo "Id number";
                                    }
                                ?>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="form-label-group">     
                            
                        <select class="form-control" name="house_no" id="house">
                            <option value="">Select House</option>
                            <?php
                                foreach ($result as $data){
                            ?>
                            <option value="<?= $data->id?>"><?= $data->house_name?></option>
                            <?php
                                }
                            ?>
                            
                            
                            
                        </select>
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-label-group">     
                            
                            <select class="form-control" name="flat_no" id="flatshow">
                                <option>Flat No</option>
                            </select>
                            
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="form-group">
                <div class="form-row">    
                    <div class="col-md-6">
                        <div class="form-label-group">
                            
                            <select class="form-control" name="status">                                
                                <option value="1">Active</option>
                                <option value="0">Deactive</option>                                
                            </select>
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-label-group">
                            <input name="start_date" id="start_date" type="date" class="form-control" placeholder="Start Date">
                            <label for="start_date">                                
                                <?php
                                    if(form_error('start_date')){
                                        echo "<span class='text-danger'>".form_error('start_date')."</span>";
                                    }else{
                                        echo "Start Date";
                                    }
                                ?>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-label-group">
                    <input name="email" id="email" type="text" class="form-control" placeholder="Email address">
                    <label for="email">                        
                        <?php
                            if(form_error('email')){
                                echo "<span class='text-danger'>".form_error('total_members')."</span>";
                            }else{
                                echo "Email address";
                            }
                        ?>
                    </label>
                </div>
            </div>
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="form-label-group">
                            <input name="user_image"  type="file" accept="image/*" class="form-control user_image_file">
                            <label>User Image</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-label-group">
                            <input name="nid" type="file" accept="image/*" class="form-control user_image_file">
                            <label>Nid Scan</label>
                        </div>
                    </div>
                </div>
            </div>
            <button name="submit" type="submit" class="btn btn-primary btn-block">Register</button>
        </form>
        
    </div>
</div>

<script>
 $(document).on("change",".fuser_image_file",function(){ 
     var result=false;
    var ext = this.value.match(/\.(.+)$/)[1];
    switch (ext) {
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'gif':
           result=true;
            break;
        default:
             result=false;
            alert('This is not an allowed file type.');
            this.value = '';
    }
    return result;
})

$('#house').on('change', function() {  
  var houseId = $(this).val();
  //alert(houseId);
  var url = '<?= base_url()?>Admin/getFlatData';
  if(houseId == ''){
        $("#flatshow").html('<option>Flat No</option>');
  } else {
      $.ajax({
        type: "POST",
        url: url,
        data:  {houseId: houseId},
        dataType: "json",
        success: function(data){
            $("#flatshow").html(data);
        } 
    });
  }
  
});
    
</script>    