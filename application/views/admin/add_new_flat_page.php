
<div class="card card-register mx-auto mt-5">
    <div class="card-header">Add New Flat</div>
    
    <div class="card-body">
        <form action="" method="POST">
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-6">                        
                        <div class="form-label-group">                                
                            <input name="flat_name" id="flat_name" type="text" class="form-control" placeholder="flat name">
                            
                            <label for="flat_name">
                                <?php
                                    if(form_error('flat_name')){
                                        echo "<span class='text-danger'>".form_error('flat_name')."</span>";
                                    }else{
                                        echo "flat name";
                                    }
                                ?>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6">                        
                        <div class="form-label-group">                                
                            <select class="form-control" name="house_id"> 
                                <option value="0">Select</option value="0">
                                <?php
                                    foreach ($house as $data){
                                ?>
                                <option value="<?= $data->id?>"><?= $data->house_name?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            
            <button name="submit" type="submit" class="btn btn-primary btn-block">Add</button>
        </form>
        
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
 $(document).on("change",".fuser_image_file",function(){ 
     var result=false;
    var ext = this.value.match(/\.(.+)$/)[1];
    switch (ext) {
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'gif':
           result=true;
            break;
        default:
             result=false;
            alert('This is not an allowed file type.');
            this.value = '';
    }
    return result;
})
    
</script>    