<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function () {
        $("#paymentDate").datepicker({dateFormat: 'yy-mm-dd'});
    });
</script>
<style type="text/css">
    div.user_info ul li{
        list-style-type: none;
        padding-bottom: 40px;
        font-size: 19px;
        font-weight: bold;
    }
</style>

<div class="card mb-3">
    <div class="card-header">
        User Profile : <img src="<?= base_url() ?>assets/images/roundgreen-1.gif_c200" width="15"> Active
    </div>
    <div class="card-body"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
        <div style="min-height: 683px;">

            <div class="row">

                <div class="col-lg-2">
                    <img class="img-thumbnail" style="border: 3px solid #d2d2d2; margin-bottom: 3px;" src="<?= base_url() ?>assets/images/users_images/<?= $basicInfo->user_image; ?>">
                    <button data-toggle="modal" data-target=".modal_for_bill_pay" style="width: 100%;" class="btn btn-danger billPayButton">Bill Pay</button>
                    <button data-toggle="" data-target="" style="width: 100%; margin-top: 2px;" class="btn btn-success">Send Email</button>                    
                    <button data-toggle="" data-target="" style="width: 100%; margin-top: 2px; color: #fff;" class="btn btn-secondary">
                        <?php
                        $bday = new DateTime($basicInfo->start_date);
                        $today = new DateTime();
                        $diff = $today->diff($bday);
                        echo $diff->y . " Year, " . $diff->m . " Month, " . $diff->d . " Days";
                        ?>
                    </button>
                    <button style="width: 100%; margin-top: 2px;" class="btn btn-info" onclick="printData()">Print Bill</button>
                </div>
                <div class="col-lg-7">
                    <div class="row">                       
                        <div class="col-lg-6">
                            <b>Basic Info</b>
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th scope="row">Name</th>
                                    <td><?= $basicInfo->name; ?></td>                                
                                </tr>
                                <tr>
                                    <th scope="row">Contact no</th>
                                    <td><?= $basicInfo->phone; ?></td>                                
                                </tr>
                                <tr>
                                    <th scope="row">Email</th>
                                    <td><?= $basicInfo->email; ?></td>                                
                                </tr>
                                <tr>
                                    <th scope="row">Members</th>
                                    <td><?= $basicInfo->total_member; ?></td>                                
                                </tr>
                                <tr>
                                    <th scope="row">Nid no</th>
                                    <td><?= $basicInfo->id_number; ?></td>                                
                                </tr>
                                <tr>
                                    <th scope="row">House</th>
                                    <td><?= $basicInfo->house_name; ?></td>                                
                                </tr>
                                <tr>
                                    <th scope="row">Flat</th>
                                    <td><?= $basicInfo->flat_no; ?></td>                                
                                </tr>
                                <tr>
                                    <th scope="row">Starting Date</th>
                                    <td><?= $basicInfo->start_date; ?></td>                                
                                </tr>
                                <tr>
                                    <th scope="row">Ending Date</th>
                                    <td><?= $basicInfo->end_date; ?></td>                                
                                </tr>

                            </table>
                        </div>
                        <div class="col-lg-6">
                            <b>Payment Info</b>
                            <table class="table table-bordered">
                                <tr>
                                    <th scope="row">House Rent</th>
                                    <td class="text-right house_rent_show"><?= $gettingData ? $gettingData->house_rent : "0"; ?></td>                                
                                </tr>
                                <tr>
                                    <th scope="row">Water Bill</th>
                                    <td class="text-right water_bill_show"><?= $gettingData ? $gettingData->water_bill : "0"; ?></td>                                
                                </tr>
                                <tr>
                                    <th scope="row">Gass Bill</th>
                                    <td class="text-right gass_bill_show"><?= $gettingData ? $gettingData->gass_bill : "0"; ?></td>                                
                                </tr>
                                <tr>
                                    <th scope="row">Electricity Bill</th>
                                    <td class="text-right electricity_bill_show"><?= $gettingData ? $gettingData->electricity_bill : "0"; ?></td>                                
                                </tr>
                                <tr>
                                    <th scope="row">Others Bill</th>
                                    <td class="text-right others_bill_show"><?= $gettingData ? $gettingData->others_bill : "0"; ?></td>                                
                                </tr>
                                <tr>
                                    <th scope="row">Advance</th>
                                    <td class="text-right advance_show"><?= $gettingData ? $gettingData->advance : "0"; ?></td> 
                                <input id="default_id" type="hidden" value="<?= $default_id; ?>">
                                </tr>
                                <tr>
                                    <th scope="row">Electric Meter Account</th>
                                    <td class="text-right electric_meter_ac_show"><?= $gettingData ? $gettingData->electric_meter_ac: "0"; ?></td>                                
                                </tr>                                

                                <tr>
                                    <th scope="row">Electric Meter no</th>
                                    <td class="text-right electric_meter_no_show"><?= $gettingData ? $gettingData->electric_meter_no: "0"; ?></td> 
                                </tr>
                            </table>

                            <button class="btn btn-info" data-toggle="modal" data-target=".modal_for_payment_update">Info Update</button>
                        </div>



                    </div> 
                </div>
                <div class="col-lg-3">
                    <img class="img-thumbnail" style="border: 3px solid #d2d2d2;" src="<?= base_url() ?>assets/images/users_images/<?= $basicInfo->user_nid; ?>">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="card mb-3">
                        <div class="card-header">
                            <i class="fas fa-table"></i>
                            Payment History</div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>House Rent</th>
                                            <th>Water Bill</th>                                            
                                            <th>Gass Bill</th>
                                            <th>Electricity Bill</th>
                                            <th>Others Bill</th>
                                            <th>Total Bill</th>
                                            <th>Billing Month</th>
                                            <th>Notes</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($tableData as $tableDatas) {
                                            ?>
                                            <tr>
                                                <td><?= $tableDatas->insert_date ?></td>
                                                <td><?= $tableDatas->insert_house_rent ?></td>
                                                <td><?= $tableDatas->insert_water_bill ?></td>
                                                <td><?= $tableDatas->insert_gass_bill ?></td>
                                                <td><?= $tableDatas->insert_electricity_bill ?></td>
                                                <td><?= $tableDatas->insert_others_bill ?></td>                                                
                                                <td><?= $tableDatas->insert_total ?></td>                                                
                                                <td><?= $tableDatas->insert_billing_month ?></td>                                                
                                                <td><?= $tableDatas->insert_note ?></td>                                                
                                            </tr>
                                            <?php
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>



        </div>
    </div>
    <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>
<div class="modal fade modal_for_payment_update" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Payment Information Update</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <table class="table table-bordered">
                        <tr>
                            <th scope="row">House Rent</th>
                            <td class="text-right"><input id="house_rent" type="text" value="<?= $gettingData ? $gettingData->house_rent : "0"; ?>"></td>                                
                        </tr>
                        <tr>
                            <th scope="row">Water Bill</th>
                            <td class="text-right"><input id="water_bill" type="text" value="<?= $gettingData ? $gettingData->water_bill : "0"; ?>"></td>                                
                        </tr>
                        <tr>
                            <th scope="row">Gass Bill</th>
                            <td class="text-right"><input id="gass_bill" type="text" value="<?= $gettingData ? $gettingData->gass_bill : "0"; ?>"></td>                                
                        </tr>
                        <tr>
                            <th scope="row">Electricity Bill</th>
                            <td class="text-right"><input id="electricity_bill" type="text" value="<?= $gettingData ? $gettingData->electricity_bill : "0"; ?>"></td>                                
                        </tr>
                        <tr>
                            <th scope="row">Others Bill</th>
                            <td class="text-right"><input id="others_bill" type="text" value="<?= $gettingData ? $gettingData->others_bill : "0"; ?>"></td>                                
                        </tr>
                        <tr>
                            <th scope="row">Advance</th>
                            <td class="text-right"><input id="advance" type="text" value="<?= $gettingData ? $gettingData->advance : "0"; ?>"></td>                                
                        </tr>
                        <tr>
                            <th scope="row">Electric Meter Account</th>
                            <td class="text-right"><input id="electric_meter_ac" type="text" value="<?= $gettingData ? $gettingData->electric_meter_ac : "0"; ?>"></td>                                
                        </tr>
                        <tr>
                            <th scope="row">Electric Meter no</th>
                            <td class="text-right"><input id="electric_meter_no" type="text" value="<?= $gettingData ? $gettingData->electric_meter_no : "0"; ?>"></td>                                
                        </tr>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="updateData" class="btn btn-primary" data-dismiss="modal">Update</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade modal_for_bill_pay" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Payment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <table class="table table-bordered" id="paymentdetails">
                        <tr>
                            <th scope="row">House Rent</th>
                            <td class="text-right house_rent_show insert_house_rent" id="house_rent_show"><?= $gettingData ? $gettingData->house_rent : "0"; ?></td>                                
                        </tr>
                        <tr>
                            <th scope="row">Water Bill</th>
                            <td class="text-right water_bill_show insert_water_bill" id="water_bill_show"><?= $gettingData ? $gettingData->water_bill : "0"; ?></td>                                
                        </tr>
                        <tr>
                            <th scope="row">Gass Bill</th>
                            <td class="text-right gass_bill_show insert_gass_bill" id="gass_bill_show"><?= $gettingData ? $gettingData->gass_bill : "0"; ?></td>                                
                        </tr>
                        <tr>
                            <th scope="row">Electricity Bill</th>
                            <td class="text-right electricity_bill_show insert_electricity_bill" id="electricity_bill_show"><?= $gettingData ? $gettingData->electricity_bill : "0"; ?></td>                                
                        </tr>
                        <tr>
                            <th scope="row">Others Bill</th>
                            <td class="text-right others_bill_show insert_others_bill" id="others_bill_show"><?= $gettingData ? $gettingData->others_bill : "0"; ?></td>                                
                        </tr>

                        <tr>
                            <th scope="row">Total</th>
                            <td class="text-right total insert_total" id="totalAmount"></td>                                
                        </tr>
                        <tr>
                            <th scope="row">Billing month</th>
                            <td class="text-right total" id="totalAmount">
                                <select class="form-control insert_billing_month">
                                    <option value="January">January</option>
                                    <option value="February">February</option>
                                    <option value="March">March</option>
                                    <option value="April">April</option>
                                    <option value="May">May</option>
                                    <option value="June">June</option>
                                    <option value="July">July</option>
                                    <option value="August">August</option>
                                    <option value="September">September</option>
                                    <option value="October">October</option>
                                    <option value="November">November</option>
                                    <option value="December">December</option>                                
                                </select>
                            </td>                                
                        </tr>
                        <tr>
                            <th scope="row">Date</th>
                            <td class="text-right total" id="totalAmount"><input class="form-control insert_date" id="paymentDate" type="text" value=""></td>                                
                        </tr>
                        <tr>
                            <th scope="row">Note</th>
                            <td class="text-right total" id="totalAmount"><textarea class="form-control insert_note"></textarea></td>                                
                        </tr>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="billPay" class="btn btn-primary" data-dismiss="modal">Pay</button>
                <button type="button" onclick="printData()" class="btn btn-success" data-dismiss="modal">Print</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on('click', '#updateData', function () {

        var house_rent = $('#house_rent').val();
        if (house_rent.length < 1) {
            house_rent = 0
        }
        var water_bill = $('#water_bill').val();
        if (water_bill.length < 1) {
            water_bill = 0
        }
        var gass_bill = $('#gass_bill').val();
        if (gass_bill.length < 1) {
            gass_bill = 0
        }
        var electricity_bill = $('#electricity_bill').val();
        if (electricity_bill.length < 1) {
            electricity_bill = 0
        }
        var others_bill = $('#others_bill').val();
        if (others_bill.length < 1) {
            others_bill = 0
        }
        var advance = $('#advance').val();
        if (advance.length < 1) {
            advance = 0
        }
        var default_id = $('#default_id').val();
        
        var electric_meter_ac = $('#electric_meter_ac').val();
        if(electric_meter_ac.length < 1){
            electric_meter_ac = 0
        }
        
        var electric_meter_no = $('#electric_meter_no').val();
        //alert(electric_meter_no);
        if(electric_meter_no.length < 1){
            electric_meter_no = 0
        }
        
        
        $.ajax({
            url: '<?= base_url() ?>/admin/update_user_profile_payment',
            type: 'POST',
            data: {
                house_rent: house_rent,
                water_bill: water_bill,
                gass_bill: gass_bill,
                electricity_bill: electricity_bill,
                others_bill: others_bill,
                advance: advance,
                default_id: default_id,
                electric_meter_ac: electric_meter_ac,
                electric_meter_no: electric_meter_no
            },
            dataType: "json",
            success: function (response) {
                if (response) {
                    $(".house_rent_show").html(house_rent);
                    $(".water_bill_show").html(water_bill);
                    $(".gass_bill_show").html(gass_bill);
                    $(".electricity_bill_show").html(electricity_bill);
                    $(".others_bill_show").html(others_bill);
                    $(".electric_meter_ac_show").html(electric_meter_ac);
                    $(".electric_meter_no_show").html(electric_meter_no);
                } else {
                }
            }

        });
    });
    
    

    // payment transection making
    $(document).on('click', '#billPay', function () {
        var insert_house_rent = $(".insert_house_rent").text();
        var insert_water_bill = $(".insert_water_bill").text();
        var insert_gass_bill = $(".insert_gass_bill").text();
        var insert_electricity_bill = $(".insert_electricity_bill").text();
        var insert_others_bill = $(".insert_others_bill").text();
        var insert_total = $(".insert_total").text();
        var insert_billing_month = $(".insert_billing_month").val();
        var insert_date = $("#paymentDate").val();
        var insert_note = $(".insert_note").val();
        var defaultID = $("#default_id").val();
        //alert(defaultID);

        $.ajax({
            url: '<?= base_url() ?>/admin/payment',
            type: 'POST',
            data: {
                insert_house_rent: insert_house_rent,
                insert_water_bill: insert_water_bill,
                insert_gass_bill: insert_gass_bill,
                insert_electricity_bill: insert_electricity_bill,
                insert_others_bill: insert_others_bill,
                insert_total: insert_total,
                insert_billing_month: insert_billing_month,
                insert_date: insert_date,
                insert_note: insert_note,
                defaultID: defaultID

            },
            dataType: "json",
            success: function (response) {
                //alert('test ok');
            }});
        
        $('#dataTable').DataTable().destroy();
        
  
        
        $('#dataTable').DataTable( {
         ajax: {
            url: "<?= base_url()?>admin/dataTable_data",
            type: 'POST',
            data:{defaultID:defaultID},
            dataType: 'json',
            },
        columns: [
            { data: 'insert_date' },
            { data: 'insert_house_rent' },
            { data: 'insert_water_bill' },
            { data: 'insert_gass_bill' },
            { data: 'insert_electricity_bill' },
            { data: 'insert_others_bill' },            
            { data: 'insert_total' },
            { data: 'insert_billing_month' },
            { data: 'insert_note' }
        ]
    } ).draw();

    });

    $(document).on('click', '.billPayButton', function () {
        var house_rent_show = $("#house_rent_show").text();
        var water_bill_show = $("#water_bill_show").text();
        var gass_bill_show = $("#gass_bill_show").text();
        var electricity_bill_show = $("#electricity_bill_show").text();
        var others_bill_show = $("#others_bill_show").text();

        var total = parseInt(house_rent_show) + parseInt(water_bill_show) + parseInt(gass_bill_show) + parseInt(electricity_bill_show) + parseInt(others_bill_show);

        $('#totalAmount').html(total);
    });
    
    function printData()
    {
       var divToPrint=document.getElementById("paymentdetails");
       newWin= window.open("");
       newWin.document.write(divToPrint.outerHTML);
       newWin.print();
       newWin.close();
    }

</script>