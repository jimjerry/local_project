
<div class="card card-register mx-auto mt-5">
    <div class="card-header">Add New House</div>
    
    <div class="card-body">
        <form action="" method="POST">
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-6">                        
                        <div class="form-label-group">                                
                            <input name="house_name" id="house_name" type="text" class="form-control" placeholder="Name">
                            
                            <label for="house_name">
                                <?php
                                    if(form_error('house_name')){
                                        echo "<span class='text-danger'>".form_error('house_name')."</span>";
                                    }else{
                                        echo "Name";
                                    }
                                ?>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-label-group">
                            <textarea name="house_address" id="house_address" type="text" class="form-control" placeholder="Address"></textarea>
                            <label for="house_address">
                                <?php
                                    if(form_error('house_address')){
                                        echo "<span class='text-danger'>".form_error('house_address')."</span>";
                                    }
                                ?>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <button name="submit" type="submit" class="btn btn-primary btn-block">Add</button>
        </form>
        
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
 $(document).on("change",".fuser_image_file",function(){ 
     var result=false;
    var ext = this.value.match(/\.(.+)$/)[1];
    switch (ext) {
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'gif':
           result=true;
            break;
        default:
             result=false;
            alert('This is not an allowed file type.');
            this.value = '';
    }
    return result;
})
    
</script>    