<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script>
  $( function() {
      $( "#start_datePick" ).datepicker({
          
          dateFormat: 'yy-mm-dd',//check change
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+0",
          
      });
    
  } );
  $( function() {
      $( "#end_datePick" ).datepicker({
          
          dateFormat: 'yy-mm-dd',//check change
            changeMonth: true,
            changeYear: true,
            yearRange: "-100:+0",
          
      });
    
  } );
  </script>
<div class="card card-register mx-auto mt-5">
    <div class="card-header">Edit User Information</div>    
    <div class="">
        <div class="card-body">
            <?php
            $error_image='';
                if($error_image){
                    echo $error_image."<br><br>";
                }
            ?>
            <form action="" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">                        
                            <div class="form-label-group">  
                                <?php
                                    //foreach ($result as $results){
                                ?>
                                <input name="name" type="text" value="<?=$results->name;?>" class="form-control">
                                
                                <label for="name">
                                    <?php
                                    if (form_error('name')) {
                                        echo "<span class='text-danger'>" . form_error('name') . "</span>";
                                    } else {
                                        echo "Name";
                                    }
                                    ?>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input name="total_members" value="<?=$results->total_member;?>" type="text" class="form-control">
                                <label for="total_members">
                                    <?php
                                    if (form_error('name')) {
                                        echo "<span class='text-danger'>" . form_error('total_members') . "</span>";
                                    } else {
                                        echo "Total Members";
                                    }
                                    ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input value="<?=$results->phone;?>" name="phone" type="text" class="form-control">
                                <label for="phone">
                                    <?php
                                    if (form_error('phone')) {
                                        echo "<span class='text-danger'>" . form_error('total_members') . "</span>";
                                    } else {
                                        echo "Phone";
                                    }
                                    ?>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input value="<?=$results->id_number;?>" name="id_number" type="text" class="form-control">
                                <label for="id_number">                                
                                    <?php
                                    if (form_error('id_number')) {
                                        echo "<span class='text-danger'>" . form_error('total_members') . "</span>";
                                    } else {
                                        echo "Id number";
                                    }
                                    ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-label-group">     

                                <select class="form-control" name="house_no" id="UpdateHouse">
                                    <?php
                                        foreach ($house_info as $house){
                                    ?>
                                    <option value="<?= $house->id?>" <?= ($house->id == $results->house_no)? "selected='selected'": "" ?>><?= $house->house_name?></option>
                                    
                                    <?php
                                        }
                                    ?>


                                </select>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-label-group">     

                                <select class="form-control" name="flat_no" id="UpdateFlatshow">
                                    <option value="<?= $results->flat_no?>"><?= $results->flat_no?></option>
                                    <?php
                                        foreach ($flat_info as $flat){
                                    ?>
                                        <option value="<?= $flat->flat_name?>"><?= $flat->flat_name?></option>
                                    
                                    <?php
                                        }
                                    ?>
                                  
                                </select>

                            </div>
                        </div>                    
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">    
                        <div class="col-md-4">
                            <div class="form-label-group">
                                
                                <select class="form-control" name="status"> 
                                    <?php
                                        if($results->status == 1){
                                    ?>
                                    <option value="1" selected="selected">Active</option>
                                    <option value="0">Deactive</option>
                                    <?php
                                        }else{
                                    ?>
                                    <option value="1">Active</option>
                                    <option value="0" selected="selected">Deactive</option>  
                                    <?php
                                        }
                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-label-group">
                                <input name="start_date" id="start_datePick" value="<?= $results->start_date?>" type="text" class="form-control">
                                <label for="start_date">                                
                                    <?php
                                    if (form_error('start_date')) {
                                        echo "<span class='text-danger'>" . form_error('start_date') . "</span>";
                                    } else {
                                        echo "Start Date";
                                    }
                                    ?>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-label-group">
                                <input name="end_date" type="text" id="end_datePick" value="<?= $results->end_date;?>" class="form-control">
                                <label for="end_date">                                
                                    <?php
                                    if (form_error('end_date')) {
                                        echo "<span class='text-danger'>" . form_error('end_date') . "</span>";
                                    } else {
                                        echo "End Date";
                                    }
                                    ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                        <input value="<?=$results->email;?>" name="email" type="text" class="form-control">
                        <label for="email">                        
                            <?php
                            if (form_error('email')) {
                                echo "<span class='text-danger'>" . form_error('total_members') . "</span>";
                            } else {
                                echo "Email address";
                            }
                            ?>
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input name="user_image"  type="file" accept="image/*" value="<?=$results->user_image;?>" class="form-control user_image_file">
                                <label>User Image</label>
                                <img class="img-thumbnail" src="<?= base_url()."assets/images/users_images/".$results->user_image?>">
                            </div>
                            
                        </div>
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input name="nid" type="file" accept="image/*" value="<?=$results->user_nid;?>" class="form-control user_image_file">
                                <label>Nid Scan</label>
                                <img class="img-thumbnail" src="<?= base_url()."assets/images/users_images/".$results->user_nid?>">
                            </div>
                        </div>
                    </div>
                </div>
                <button name="submit" type="submit" class="btn btn-primary btn-block">Register</button>
            </form>
            <?php
              //  }
            ?>
        </div>
    </div>
</div>

<script>
$('#UpdateHouse').on('change', function() {  
  //alert(this.value);
  
  var houseId = $(this).val();
  //alert(houseId);
  var url = '<?= base_url()?>Admin/getFlatData';
  if(houseId == ''){
        $("#UpdateFlatshow").html('<option>Flat No</option>');
  } else {
      $.ajax({
        type: "POST",
        url: url,
        data:  {houseId: houseId},
        dataType: "json",
        success: function(data){
            $("#UpdateFlatshow").html(data);
        } 
    });
  }
  
});
</script>